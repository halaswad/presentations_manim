from manim import *
from manim_slides import Slide

# manim-slides First Introduction NFS Frobenius Problematic Factory_idea Complete_diagram Example Results Conclusion


def get_diagrame():
    diag = VGroup()
    ZX = Tex("$\mathbb{Z}[X]$").to_edge(UP, buff=1)
    K0 = Tex("$ \mathcal{K}_0 = \mathbb{Q}[X]/(f_0)$").next_to(ZX, DL, buff=2)
    K1 = Tex("$ \mathcal{K}_1 = \mathbb{Q}[X]/(f_1)$").next_to(ZX, DR, buff=2)
    Fpn = Tex("$ \mathbb{F}_{p^n} = \mathbb{F}_{p}[X]/ (\psi)$").next_to(
        ZX, DOWN, buff=4
    )
    arrow_1 = Line(start=ZX.get_bottom(), end=K0.get_center(), buff=1).add_tip()
    arrow_2 = Line(start=ZX.get_bottom(), end=K1.get_center(), buff=1).add_tip()
    arrow_3 = Line(start=K0.get_bottom(), end=Fpn.get_center(), buff=1).add_tip()
    arrow_4 = Line(start=K1.get_bottom(), end=Fpn.get_center(), buff=1).add_tip()

    diag.add(ZX, K0, K1, Fpn, arrow_1, arrow_2, arrow_3, arrow_4)
    return diag


def get_diagrame_bis():
    diag = VGroup()
    ZX = Tex("$\mathbb{Z}[X]$").to_edge(UP, buff=1)
    K0 = Tex("$ \mathcal{K}_0 = \mathbb{Q}[X]/(f_0)$").next_to(ZX, DL, buff=2)
    K1 = Tex("$ \mathcal{K}_1 = \mathbb{Q}[X]/(f_1)$").next_to(ZX, DR, buff=2)
    Fpn = Tex("$ \mathbb{F}_{p^n} = \mathbb{F}_{p}[X]/ (\psi)$").next_to(
        ZX, DOWN, buff=4
    )
    arrow_1 = always_redraw(
        lambda: Line(start=ZX.get_bottom(), end=K0.get_top(), buff=0.4).add_tip()
    )
    arrow_2 = Line(start=ZX.get_bottom(), end=K1.get_top(), buff=0.4).add_tip()
    arrow_3 = Line(start=K0.get_bottom(), end=Fpn.get_top(), buff=0.4).add_tip()
    arrow_4 = Line(start=K1.get_bottom(), end=Fpn.get_top(), buff=0.4).add_tip()

    diag.add(ZX, K0, K1, Fpn, arrow_1, arrow_2, arrow_3, arrow_4)
    return diag


class First(Slide):
    def construct(self):
        title = Title("Discrete Logarithm Factory", color=YELLOW)

        author = Tex("Haetham Al Aswad")
        inst = Tex("Inria - Nancy")
        points_1 = Tex(
            "----------------------------------------------------------------",
            color=YELLOW,
        )

        joint_work = Tex("Joint work with Cécile Pierrot and Emmanuel Thomé")
        points_2 = Tex(
            "----------------------------------------------------------------",
            color=YELLOW,
        )
        date = Tex("Journées C2: october 2023")
        info = (
            VGroup(author, inst, points_1, joint_work, points_2, date)
            .arrange(DOWN)
            .scale(0.6)
            .shift(UP)
        )

        logo_inria = ImageMobject("inria_rouge_rvb.png").scale(0.5)
        logo_loria = ImageMobject("logoloria.png").scale(0.3)
        logo_ul = ImageMobject("ul_logo.png").scale(0.9)
        logo_aid = ImageMobject("LOGO_AID.png").scale(0.9)
        logo_caramba = ImageMobject("caramba.png").scale(0.5)
        logos = (
            Group(logo_inria, logo_loria, logo_ul, logo_aid, logo_caramba)
            .arrange(RIGHT)
            .scale(0.7)
            .to_edge(DOWN)
        )

        self.add(title, info, logos)
        self.wait()
        self.next_slide()


class Introduction(Slide):
    def construct(self):
        title_intro = (
            Title(
                "Discrete Logarithm in Finite Fields",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        title_problem = Title(
            "Problem",
            include_underline=True,
            fill_color=YELLOW,
            match_underline_width_to_text=True,
        ).scale(0.6 / 0.8)
        finite_field = MathTex(r"\mathbb{F}_{p^n}^* = \langle g \rangle")
        challenge = Tex("$T = g^s$")
        challenge[0][3:4].set_color(RED)
        problem = (
            VGroup(title_problem, finite_field, challenge)
            .arrange(DOWN)
            .scale(0.8)
            .to_edge(UL, buff=2)
        )

        title_primitives = (
            Title(
                "Primitives",
                include_underline=True,
                fill_color=YELLOW,
                match_underline_width_to_text=True,
            )
            .scale(0.6)
            .next_to(title_problem, RIGHT)
            .shift(6 * RIGHT)
        )

        primitives = BulletedList(
            "Diffie-Hellman key exchange.",
            "ElGamal primitives.",
            "Pairing-Based-Cryptography.",
            "Digital Signature Algorithm",
        )

        primitives_group = (
            VGroup(primitives).scale(0.6).arrange(DOWN).next_to(title_primitives, DOWN)
        )

        # chara_size = (
        #    Line(start=LEFT, end=RIGHT, color=YELLOW).scale(6).to_edge(DOWN, buff=2)
        # )
        # pt1 = Point().scale(3).next_to(chara_size.get_start(), 16 * RIGHT)
        # pt2 = Point().scale(3).next_to(chara_size.get_start(), 32 * RIGHT)

        line_factor = 2

        medium_line = (
            Line(start=LEFT, end=RIGHT).scale(line_factor).to_edge(DOWN, buff=2)
        )
        medium_txt = Tex("Medium characteristic").scale(0.7).next_to(medium_line, DOWN)
        medium = VGroup(medium_line, medium_txt)

        small_line = (
            Line(start=LEFT, end=RIGHT)
            .scale(line_factor)
            .next_to(medium_line, LEFT, buff=-0.01)
        )
        small_txt = Tex("Small characteristic").scale(0.7).next_to(small_line, DOWN)
        small_field = Tex("$\mathbb{F}_{2^n}$").next_to(small_line.get_start(), UP)
        small = VGroup(small_line, small_txt, small_field)
        cross = Cross().move_to(small.get_center())

        large_line = (
            Line(start=LEFT, end=RIGHT)
            .scale(line_factor)
            .next_to(medium_line, RIGHT, buff=-0.01)
        )
        large_txt = Tex("Large characteristic").scale(0.7).next_to(large_line, DOWN)
        prime_field = Tex("$\mathbb{F}_{p}$").next_to(large_line.get_end(), UP)
        large_field_1 = Tex("$\mathbb{F}_{p^2}$").next_to(prime_field, 2 * LEFT)
        large_field_2 = Tex("$\mathbb{F}_{p^6}$").next_to(large_field_1, 6 * LEFT)
        large = VGroup(large_line, large_txt, prime_field, large_field_1, large_field_2)

        self.add(title_intro, problem, title_primitives, primitives_group)
        self.wait()
        self.next_slide()
        self.add(small, cross, medium, large)
        self.wait()
        self.next_slide()


class NFS(Slide):
    def construct(self):
        title_nfs = (
            Title(
                "Number Field Sieve (NFS)",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        diag = get_diagrame().scale(0.8).to_edge(UP)
        self.add(title_nfs)
        self.add(diag)
        self.wait()

        # Polynomial selection
        self.next_slide()
        text_poly = (
            Text("- Polynomial Selection")
            .next_to(title_nfs, DOWN, buff=-0.2)
            .scale(0.4)
            .align_to(title_nfs, LEFT)
        )
        self.play(Write(text_poly))
        text_poly_condition_1 = "$f_0$ and $f_1$ are co-prime, irreducible, and share $\psi$ irreducible of degree $n$ mod $p$"
        text_poly_condition_2 = (
            "$f_0$ and $f_1$ have small degree and coefficient sizes"
        )
        text_poly_condition = (
            BulletedList(text_poly_condition_1, text_poly_condition_2)
            .scale(0.7)
            .next_to(diag, DOWN)
        )
        self.next_slide()
        self.add(text_poly_condition)
        self.wait()

        # Relation Collection
        self.next_slide()
        self.play(FadeOut(text_poly_condition))
        text_rel = (
            Text("- Relation Collection")
            .next_to(text_poly, DOWN, buff=-0.1)
            .scale(0.4)
            .align_to(text_poly, LEFT)
        )
        self.play(Write(text_rel))
        self.next_slide()
        phi_0 = Tex("$\phi_0$").next_to(diag[0], DOWN)
        self.play(FadeIn(phi_0))
        self.next_slide()
        phi_00 = Tex("$\phi_{00}$").move_to(phi_0.get_center())
        self.play(phi_00.animate.next_to(diag[1], RIGHT))
        self.next_slide()
        b_smooth = Text("B-smooth", color=GREEN).scale(0.3)
        b_smooth_2 = Text("B-smooth", color=GREEN).scale(0.3)
        not_b_smooth = Text("Not B-smooth", color=RED).scale(0.3)
        self.play(
            FadeIn(not_b_smooth.next_to(phi_00, UP)),
            phi_00.animate.set_color(RED),
        )
        self.next_slide()
        self.play(FadeOut(phi_0, phi_00, not_b_smooth))
        phi_1 = Tex("$\phi_1$").next_to(diag[0], DOWN)
        self.play(FadeIn(phi_1))
        phi_10 = Tex("$\phi_{10}$").move_to(phi_1.get_center())
        self.play(phi_10.animate.next_to(diag[1], RIGHT))
        self.play(
            FadeIn(b_smooth.next_to(phi_10, UP)),
            phi_10.animate.set_color(GREEN),
        )
        self.next_slide()
        phi_11 = Tex("$\phi_{11}$").move_to(phi_1.get_center())
        self.play(phi_11.animate.next_to(diag[2], LEFT))
        self.play(
            FadeIn(not_b_smooth.next_to(phi_11, UP)),
            phi_11.animate.set_color(RED),
        )
        self.next_slide()
        self.play(FadeOut(phi_1, phi_10, phi_11, b_smooth, not_b_smooth))
        phi_2 = Tex("$\phi_2$").next_to(diag[0], DOWN)
        self.play(FadeIn(phi_2))
        phi_20 = Tex("$\phi_{20}$").move_to(phi_2.get_center())
        self.play(phi_20.animate.next_to(diag[1], RIGHT))
        self.play(
            FadeIn(b_smooth.next_to(phi_20, UP)),
            phi_20.animate.set_color(GREEN),
        )
        self.next_slide()
        phi_21 = Tex("$\phi_{21}$").move_to(phi_2.get_center())
        self.play(phi_21.animate.next_to(diag[2], LEFT))
        self.play(
            FadeIn(b_smooth_2.next_to(phi_21, UP)),
            phi_21.animate.set_color(GREEN),
        )
        self.next_slide()

        equality = Tex(" '=' ").next_to(diag, DOWN, buff=1)
        eq1 = (
            MathTex(
                r"(\phi_{20}) = \prod_{\substack{\mathfrak{P} \text{ prime ideal} \\ \text{of norm} < B}} \mathfrak{P}^{val_{\mathfrak{P}}(\phi_{20})}"
            )
            .scale(0.8)
            .next_to(equality, LEFT)
        )
        eq2 = (
            MathTex(
                r"\prod_{\substack{\mathfrak{Q} \text{ prime ideal} \\ \text{of norm} < B}} \mathfrak{Q}^{val_{\mathfrak{Q}}(\phi_{21})} = (\phi_{21})"
            )
            .scale(0.8)
            .next_to(equality, RIGHT)
        )
        self.play(
            FadeOut(b_smooth, b_smooth_2, phi_1),
            FadeTransform(phi_20, eq1),
            FadeTransform(phi_21, eq2),
        )
        self.play(Write(equality))
        self.next_slide()

        eq3 = (
            MathTex(
                r"\sum_{\substack{\mathfrak{P} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{P}}(\phi_{20}) \log \left( \mathfrak{P} \right) \quad = \sum_{\substack{\mathfrak{Q} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{Q}}(\phi_{21}) \log \left( \mathfrak{Q} \right)"
            )
            .scale(0.8)
            .move_to(equality)
        )
        eq3[0][29:35].set_color(RED)
        eq3[0][-6:-1].set_color(RED)
        eq3[0][-1].set_color(RED)

        self.play(FadeTransform(VGroup(eq1, eq2, equality), eq3))
        self.next_slide()
        self.play(eq3.animate.scale(0.5).shift(0.5 * UP))
        eq4 = (
            (
                MathTex(
                    r"\sum_{\substack{\mathfrak{P} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{P}}(\phi_{0i}) \log \left( \mathfrak{P} \right) \quad = \sum_{\substack{\mathfrak{Q} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{Q}}(\phi_{1i}) \log \left( \mathfrak{Q} \right)"
                ).scale(0.8)
            )
            .scale(0.5)
            .next_to(eq3, DOWN)
        )
        eq4[0][29:35].set_color(RED)
        eq4[0][-6:-1].set_color(RED)
        eq4[0][-1].set_color(RED)
        vertical_dots = VGroup()
        vertical_dots.add(Dot(), Dot(), Dot())
        vertical_dots.arrange(DOWN, buff=-0.5).scale(0.5).next_to(eq4, DOWN)
        eqk = (
            (
                (
                    MathTex(
                        r"\sum_{\substack{\mathfrak{P} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{P}}(\phi_{0j}) \log \left( \mathfrak{P} \right) \quad = \sum_{\substack{\mathfrak{Q} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{Q}}(\phi_{1j}) \log \left( \mathfrak{Q} \right)"
                    ).scale(0.8)
                )
            )
            .scale(0.5)
            .next_to(vertical_dots, DOWN)
        )
        eqk[0][29:35].set_color(RED)
        eqk[0][-6:-1].set_color(RED)
        eqk[0][-1].set_color(RED)

        self.play(FadeIn(eq4, vertical_dots, eqk))

        # Linear Algebra
        self.next_slide()
        text_linear_algebra = (
            Text("- Linear Algebra")
            .next_to(text_rel, DOWN, buff=-0.1)
            .scale(0.4)
            .align_to(text_rel, LEFT)
        )
        self.play(Write(text_linear_algebra))
        self.next_slide()
        self.play(FadeOut(eq3, eq4, vertical_dots, eqk))
        circle = Circle(radius=1.5).next_to(diag, DOWN)
        base = (
            Circle(radius=0.3)
            .set_color(GREEN)
            .set_fill(GREEN, opacity=1.0)
            .move_to(circle)
            .shift(0.7 * DR)
        )
        text_base = Tex("Factor basis", color=GREEN).scale(0.5).next_to(base, UP)
        self.play(FadeIn(VGroup(circle, base, text_base)))

        # Individual Logarithm
        self.next_slide()
        text_individual_logarithm = (
            Text("- Individual Logarithm")
            .next_to(text_linear_algebra, DOWN, buff=-0.1)
            .scale(0.4)
            .align_to(text_linear_algebra, LEFT)
        )
        self.play(Write(text_individual_logarithm))

        target = Text("T", color=RED).move_to(circle).shift(0.7 * UL)
        arr = CurvedArrow(target.get_bottom(), base.get_center())
        self.play(FadeIn(target))
        self.play(Create(arr))
        self.next_slide()


class Problem(Slide):
    def construct(self):
        title_problematic = (
            Title(
                "Problem",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        self.add(title_problematic)
        ant = SVGMobject("ant.svg").set_fill(WHITE).to_edge(UP)

        sheep_1 = (
            SVGMobject("sheep.svg").set_fill(WHITE).to_edge(DL, buff=2).shift(LEFT)
        )
        sheep_2 = sheep_1.copy().next_to(sheep_1, RIGHT)
        sheep_k = sheep_1.copy().to_edge(DR, buff=2).shift(RIGHT)
        dots = Tex("$\dots \dots$").to_edge(DOWN, buff=3).shift(RIGHT)

        self.play(
            DrawBorderThenFill(ant),
            DrawBorderThenFill(sheep_1),
            DrawBorderThenFill(sheep_2),
            FadeIn(dots),
            DrawBorderThenFill(sheep_k),
        )

        Fpn_1 = Tex("$ \mathbb{F}_{p_1^n}$").next_to(sheep_1, DOWN)
        Fpn_2 = Tex("$ \mathbb{F}_{p_2^n}$").next_to(sheep_2, DOWN)
        Fpn_k = Tex("$ \mathbb{F}_{p_k^n}$").next_to(sheep_k, DOWN)

        Txt = Text("All fields of roughly 1024 bits").scale(0.5).to_edge(DOWN)

        self.play(FadeIn(Fpn_1, Fpn_2, Fpn_k, Txt))
        self.next_slide()


class Factory_idea(Slide):
    def construct(self):
        title_factory = (
            Title(
                "Discrete Logarithm Factory",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        self.add(title_factory)

        diag = get_diagrame_bis().scale(0.8)
        self.add(diag)
        self.wait()
        self.next_slide()
        self.play(
            FadeOut(diag[2], diag[3], diag[5], diag[6], diag[7]),
            diag[0].animate.next_to(diag[1], UP).shift(1.5 * UP),
        )
        text_one_off = (
            Text("- One-off step")
            .next_to(title_factory, DOWN)
            .scale(0.4)
            .align_to(title_factory, LEFT)
        )
        self.play(Write(text_one_off))
        self.next_slide()

        # we replace with a new diag
        ZX = diag[0].copy()
        K0 = diag[1].copy()
        arrow_1 = Line(start=ZX.get_bottom(), end=K0.get_top(), buff=0.4).add_tip()

        self.remove(diag)
        diag = VGroup(ZX, K0, arrow_1).set_opacity(0.4)
        self.add(diag)

        text_rec = Tex("$Store_{1024}$")
        rec = Rectangle(height=0.5, width=4)
        rectangle = VGroup(text_rec, rec).arrange(DOWN).scale(0.5).to_edge(UP)
        self.play(FadeIn(rectangle))

        phi = Tex("$\phi$").next_to(diag[0], RIGHT)
        b_smooth = Text("B-smooth", color=GREEN).scale(0.3)
        not_b_smooth = Text("Not B-smooth", color=RED).scale(0.3)
        self.start_loop()
        self.play(FadeIn(phi))
        self.play(phi.animate.next_to(diag[1], RIGHT))
        not_b_smooth.next_to(phi, UP)
        self.play(phi.animate.set_color(RED), Write(not_b_smooth))
        self.play(FadeOut(phi, not_b_smooth))

        phi.set_color(WHITE).next_to(diag[0], RIGHT)
        self.play(FadeIn(phi))
        self.play(phi.animate.next_to(diag[1], RIGHT))
        b_smooth.next_to(phi, UP)
        self.play(phi.animate.set_color(GREEN), Write(b_smooth))
        self.play(phi.animate.move_to(rec.get_center()), FadeOut(b_smooth))
        self.play(FadeOut(phi))
        self.end_loop()
        self.wait()
        self.next_slide()

        K1 = (
            Tex("$ \mathcal{K}_1 = \mathbb{Q}[X]/(f_1)$")
            .next_to(diag[1], RIGHT, buff=1)
            .scale(0.8)
        )
        Fp1n = (
            Tex("$ \mathbb{F}_{p_1^n} = \mathbb{F}_{p_1}[X]/ (\psi_1)$")
            .next_to(K1, DOWN, buff=2)
            .scale(0.8)
        )
        sheep_1 = SVGMobject("sheep.svg").scale(0.5).set_fill(WHITE).next_to(Fp1n, DOWN)
        arrow_2 = (
            Line(start=diag[0].get_bottom(), end=K1.get_top(), buff=0.4)
            .scale(0.8)
            .add_tip()
        )
        arrow_3 = (
            Line(start=diag[1].get_bottom(), end=Fp1n.get_top(), buff=0.4)
            .scale(0.8)
            .add_tip()
        )
        arrow_4 = (
            Line(start=K1.get_bottom(), end=Fp1n.get_top(), buff=0.4)
            .scale(0.8)
            .add_tip()
        )
        text_per_field = (
            Text("- Per-field step")
            .next_to(text_one_off, DOWN, buff=-0.1)
            .scale(0.4)
            .align_to(title_factory, LEFT)
        )
        all_side_1 = VGroup(K1, Fp1n, sheep_1, arrow_2, arrow_3, arrow_4)

        self.play(
            FadeIn(text_per_field),
            diag.animate.set_color(BLUE),
            rectangle.animate.set_color(BLUE),
        )
        self.play(FadeIn(Fp1n, sheep_1))
        self.next_slide()
        self.play(Create(VGroup(K1, arrow_2, arrow_3, arrow_4), run_time=4))
        self.next_slide()

        phi = Tex("$\phi$").move_to(rec.get_center())
        eq = MathTex(
            r"\sum_{\substack{\mathfrak{P} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{P}}(\phi_0) \log \left( \mathfrak{P} \right) \quad = \sum_{\substack{\mathfrak{Q} \text{ prime ideal} \\ \text{of norm} < B}} val_{\mathfrak{Q}}(\phi_1) \log \left( \mathfrak{Q} \right)"
        ).scale(0.3)
        b_smooth = Text("B-smooth", color=GREEN).scale(0.3)
        not_b_smooth = Text("Not B-smooth", color=RED).scale(0.3)

        self.start_loop()
        self.play(FadeIn(phi))
        self.play(phi.animate.next_to(K1, RIGHT))
        not_b_smooth.next_to(phi, UP)
        self.play(phi.animate.set_color(RED), Write(not_b_smooth))
        self.play(FadeOut(phi, not_b_smooth))
        phi.set_color(WHITE).move_to(rec.get_center())
        self.play(FadeIn(phi))
        self.play(phi.animate.next_to(K1, RIGHT))
        b_smooth.next_to(phi, UP)
        self.play(phi.animate.set_color(GREEN), Write(b_smooth))
        eq.next_to(b_smooth, RIGHT)
        self.play(FadeIn(eq))
        self.play(FadeOut(phi, b_smooth, eq))
        self.end_loop()
        self.wait()
        self.next_slide()

        self.play(all_side_1.animate().set_opacity(0.2))

        K2 = (
            Tex("$ \mathcal{K}_2 = \mathbb{Q}[X]/(f_2)$")
            .next_to(K1, RIGHT, buff=1)
            .scale(0.8)
        )
        Fp2n = (
            Tex("$ \mathbb{F}_{p_2^n} = \mathbb{F}_{p_2}[X]/ (\psi_2)$")
            .next_to(K2, DOWN, buff=2)
            .scale(0.8)
        )
        sheep_2 = SVGMobject("sheep.svg").scale(0.5).set_fill(WHITE).next_to(Fp2n, DOWN)
        arrow_22 = (
            Line(start=diag[0].get_bottom(), end=K2.get_top(), buff=0.4)
            .scale(0.8)
            .add_tip()
        )
        arrow_33 = (
            Line(start=diag[1].get_bottom(), end=Fp2n.get_top(), buff=0.4)
            .scale(0.8)
            .add_tip()
        )
        arrow_44 = (
            Line(start=K2.get_bottom(), end=Fp2n.get_top(), buff=0.4)
            .scale(0.8)
            .add_tip()
        )
        self.play(FadeIn(Fp2n, sheep_2))
        self.next_slide()
        self.play(Create(VGroup(K2, arrow_22, arrow_33, arrow_44), run_time=4))
        self.next_slide()


class Frobenius(Slide):
    def construct(self):
        title_frobenius = (
            Title(
                "Frobenius density theorem",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        self.add(title_frobenius)

        f = (
            MathTex(r"f_0 = X^4 - X^2 - 1, \ \text{Irreducible over $\mathbb{Q}$}")
            .scale(0.7)
            .to_edge(UP)
            .shift(RIGHT)
        )
        self.add(f)
        self.wait()
        self.next_slide()

        f_mod_2 = MathTex(
            r"f_0 \equiv (X^2 + X + 1) \times (X^2 + X + 1) \mod 2 \ \rightarrow \text{Pattern (2,2) }"
        )
        f_mod_3 = MathTex(
            r"f_0 \equiv X^4 - X^2 - 1 \mod 3 \ \rightarrow \text{Pattern (4)}"
        )
        f_mod_11 = MathTex(
            r"f_0 \equiv (X+2) \times (X+9) \times (X^2+3) \mod 11 \ \rightarrow \text{Pattern (1, 1, 2)}"
        )
        fmod = (
            VGroup(f_mod_2, f_mod_3, f_mod_11).arrange(DOWN).scale(0.7).next_to(f, DOWN)
        )
        self.play(FadeIn(fmod))
        self.next_slide()
        t_occurence = Table(
            [
                [
                    "Patterns of f",
                    "Percentage of occurence over primes",
                ],
                ["(1,1,1,1)", "12.9 %"],
                ["(1, 1, 2)", "24.6 %"],
                ["(1,3)", "0.0 %"],
                ["(2, 2)", "37.5 %"],
                ("(4)", "25.0 %"),
            ]
        ).set_color(YELLOW)
        legend = Tex("Percentage over ten thousand primes picked at random")
        table_occurence = (
            VGroup(t_occurence, legend).arrange(DOWN).scale(0.4).next_to(fmod, DOWN)
        )
        self.play(FadeIn(table_occurence))
        self.next_slide()
        all_f = VGroup(f, fmod, table_occurence)
        self.play(all_f.animate.scale(0.7).to_edge(UL, buff=1).shift(LEFT))
        gal_f = MathTex(r"\mathrm{Gal}(f_0) = D_4").scale(0.7).next_to(f, RIGHT, buff=2)
        sigma_1 = MathTex(
            r"\sigma_1 = (13)(24) \ \rightarrow \text{Cycle pattern (2,2)}"
        )
        sigma_2 = MathTex(r"\sigma_2 = (1234) \ \rightarrow \text{Cycle pattern (4)}")
        sigma_3 = MathTex(r"\sigma_3 = (13) \ \rightarrow \text{Cycle pattern (1,1,2)}")
        sigma = VGroup(sigma_1, sigma_2, sigma_3).arrange(DOWN).scale(0.7)
        t_sigma = Table(
            [
                [
                    "Cycle patterns in Gal(f)",
                    "Percentage of occurence in Gal(f)",
                ],
                ["(1,1,1,1)", "12.5 %"],
                ["(1, 1, 2)", "25.0 %"],
                ["(1,3)", "0.0 %"],
                ["(2, 2)", "37.5 %"],
                ("(4)", "25.0 %"),
            ]
        ).set_color(YELLOW)
        legend_sigma = Text(" ")
        table_sigma = VGroup(t_sigma, legend_sigma).scale(0.4)

        gal_f_all = (
            VGroup(gal_f, sigma, table_sigma)
            .arrange(DOWN)
            .scale(0.7)
            .next_to(all_f, RIGHT)
            .shift(0.05 * UP)
        )

        self.play(FadeIn(gal_f))
        self.next_slide()
        self.play(FadeIn(sigma))
        self.next_slide()
        self.play(FadeIn(table_sigma))
        self.next_slide()

        title_notation = title_frob_theorem = Title(
            "Notation",
            include_underline=False,
            fill_color=YELLOW,
        )
        nt = Tex(
            "$\mathrm{Gal}(f)_k$ denotes the subset of $\mathrm{Gal}(f)$ of permutations that have $k$ in their cycle pattern."
        )
        notation = VGroup(title_notation, nt).arrange(DOWN)

        title_frob_theorem = Title(
            "Theorem",
            include_underline=False,
            fill_color=YELLOW,
        )
        th = Tex(
            "Let $f \in Z[X]$ irreducible with a non vanishing discriminant. The density of prime numbers modulo which $f$ admits an irreducible factor of degree $k$ is"
        )
        formula = MathTex(r"\frac{\# \mathrm{Gal}(f)_k}{\# \mathrm{Gal}(f)}")
        theorem = VGroup(title_frob_theorem, th, formula).arrange(DOWN)

        all_text = (
            VGroup(notation, theorem)
            .arrange(DOWN)
            .scale(0.5)
            .to_edge(DOWN)
            .shift(0.4 * DOWN)
        )

        title_notation.align_to(title_frobenius, LEFT)
        title_frob_theorem.align_to(title_frobenius, LEFT)
        self.play(FadeIn(all_text))
        self.next_slide()


class Chebotarev(Slide):
    def construct(self):
        title_chebotarev = (
            Title(
                "Chebotarev's density theorem",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        self.add(title_chebotarev)

        scale_factor = 0.6
        scale_factor_2 = 0.8

        L = MathTex(r"\mathbb{L}").to_edge(UP).shift(1.5 * LEFT).scale(scale_factor_2)
        K_0 = MathTex(r"\mathcal{K}_0").next_to(L, DOWN, buff=1).scale(scale_factor_2)
        K_mu = (
            MathTex(r"\mathcal{K}_{\mu}")
            .next_to(K_0, DOWN, buff=1)
            .scale(scale_factor_2)
        )
        K_h = (
            MathTex(r"\mathcal{K}_{h}")
            .next_to(K_mu, DOWN, buff=1)
            .scale(scale_factor_2)
        )
        Q = MathTex(r"\mathbb{Q}").next_to(K_h, DOWN, buff=1).scale(scale_factor_2)

        arrow_l = Line(start=L.get_bottom(), end=K_0.get_top(), buff=1)
        arrow_0 = Line(start=K_0.get_bottom(), end=K_mu.get_top(), buff=1)
        arrow_mu = Line(start=K_mu.get_bottom(), end=K_h.get_top(), buff=1)
        arrow_h = Line(start=K_h.get_bottom(), end=Q.get_top(), buff=1)

        deg_0 = (
            Tex("$\kappa$", color=YELLOW)
            .move_to(arrow_0.get_center())
            .shift(0.15 * RIGHT)
            .scale(scale_factor)
        )
        deg_mu = (
            Tex("$2$", color=YELLOW)
            .move_to(arrow_mu.get_center())
            .shift(0.15 * RIGHT)
            .scale(scale_factor)
        )
        deg_h = (
            Tex("$\eta$", color=YELLOW)
            .move_to(arrow_h.get_center())
            .shift(0.15 * RIGHT)
            .scale(scale_factor)
        )

        self.add(
            L,
            K_0,
            K_mu,
            K_h,
            Q,
            arrow_l,
            arrow_0,
            arrow_mu,
            arrow_h,
        )
        self.add(
            deg_0,
            deg_mu,
            deg_h,
        )
        self.wait()
        self.next_slide()

        p_0 = MathTex(r"\mathfrak{p}_0").next_to(K_0, 6 * LEFT).scale(scale_factor_2)
        p_mu = (
            MathTex(r"\mathfrak{p}_{\mu}")
            .next_to(K_mu, LEFT)
            .scale(scale_factor_2)
            .align_to(p_0, LEFT)
        )
        p_h = (
            MathTex(r"\mathfrak{p}_h")
            .next_to(K_h, LEFT)
            .scale(scale_factor_2)
            .align_to(p_0, LEFT)
        )
        p = Tex("$p $").next_to(p_h, DOWN).scale(scale_factor_2).align_to(Q, UP)
        that_works = (
            Tex("that works")
            .scale(scale_factor)
            .move_to(p.get_center())
            .shift(0.3 * DOWN)
        )

        arrow_0p = Line(start=p_0.get_bottom(), end=p_mu.get_top(), buff=1)
        arrow_mup = Line(start=p_mu.get_bottom(), end=p_h.get_top(), buff=1)
        arrow_hp = Line(start=p_h.get_bottom(), end=p.get_top(), buff=1)

        deg_0p = (
            Tex("$\kappa$", color=YELLOW)
            .move_to(arrow_0p.get_center())
            .shift(0.15 * RIGHT)
            .scale(scale_factor)
        )
        deg_mup = (
            Tex("$1$", color=YELLOW)
            .move_to(arrow_mup.get_center())
            .shift(0.15 * RIGHT)
            .scale(scale_factor)
        )
        deg_hp = (
            Tex("$\eta$", color=YELLOW)
            .move_to(arrow_hp.get_center())
            .shift(0.15 * RIGHT)
            .scale(scale_factor)
        )

        self.play(
            FadeIn(
                p_0,
                p_mu,
                p_h,
                p,
                that_works,
                arrow_0p,
                arrow_mup,
                arrow_hp,
                deg_0p,
                deg_mup,
                deg_hp,
            )
        )
        self.wait()
        self.next_slide()

        # Galois correspondance
        one = Tex("1").next_to(L, 5 * RIGHT).scale(0.7)
        GK0 = (
            MathTex(r"G^{\mathcal{K}_0}")
            .next_to(one, DOWN)
            .align_to(K_0, UP)
            .scale(scale_factor)
        )
        GKmu = (
            MathTex(r"G^{\mathcal{K}_{\mu}}")
            .next_to(one, DOWN)
            .align_to(K_mu, UP)
            .scale(scale_factor)
        )
        GKh = (
            MathTex(r"G^{\mathcal{K}_h}")
            .next_to(one, DOWN)
            .align_to(K_h, UP)
            .scale(scale_factor)
        )
        G = (
            MathTex(r"G := \mathrm{Gal}(\mathbb{L}/\mathbb{Q})")
            .next_to(one, DOWN)
            .align_to(Q, UP)
            .scale(scale_factor)
        )

        sub1 = (
            Tex("$\wedge$").move_to(midpoint(one.get_center(), GK0.get_center()))
        ).scale(scale_factor_2)
        sub2 = (
            Tex("$\wedge$").move_to(midpoint(GK0.get_center(), GKmu.get_center()))
        ).scale(scale_factor_2)
        sub3 = (
            Tex("$\wedge$").move_to(midpoint(GKmu.get_center(), GKh.get_center()))
        ).scale(scale_factor_2)
        sub4 = (
            Tex("$\wedge$").move_to(midpoint(GKh.get_center(), G.get_center()))
        ).scale(scale_factor_2)

        self.play(FadeIn(one, GK0, GKmu, GKh, G, sub1, sub2, sub3, sub4))
        self.next_slide()

        actK0 = (
            Tex("$G$ acts on $G/G^{\mathcal{K}_0}$")
            .scale(scale_factor)
            .next_to(GK0, 2 * RIGHT)
        )

        actKmu = (
            Tex("$G$ acts on $G/G^{\mathcal{K}_{\mu}}$")
            .scale(scale_factor)
            .next_to(GKmu, 2 * RIGHT)
        )

        actKh = (
            Tex("$G$ acts on $G/G^{\mathcal{K}_h}$")
            .scale(scale_factor)
            .next_to(GKh, 2 * RIGHT)
        )

        self.play(FadeIn(actK0, actKmu, actKh))
        self.next_slide()

        kappa_eta = (
            Tex("orbit pattern ($\eta \kappa$, .)")
            .scale(scale_factor)
            .next_to(actK0, 2 * RIGHT)
        )
        eta_mu = (
            Tex("orbit pattern ($\eta$, .)")
            .scale(scale_factor)
            .next_to(kappa_eta, DOWN)
            .align_to(K_mu, UP)
        )
        eta_1 = (
            Tex("orbit pattern ($\eta$)")
            .scale(scale_factor)
            .next_to(eta_mu, DOWN)
            .align_to(K_h, UP)
        )
        sigma = MathTex(r"\sigma").next_to(eta_1, DOWN).align_to(Q, UP)

        arrow_0g = Line(start=kappa_eta.get_bottom(), end=eta_mu.get_top(), buff=1)
        arrow_mug = Line(start=eta_mu.get_bottom(), end=eta_1.get_top(), buff=1)
        arrow_hg = Line(start=eta_1.get_bottom(), end=sigma.get_top(), buff=1)
        self.play(
            FadeIn(kappa_eta, eta_mu, eta_1, sigma, arrow_0g, arrow_mug, arrow_hg)
        )
        self.next_slide()
        density = (
            Tex("density of $p$ that work =", color=YELLOW)
            .to_edge(DOWN)
            .scale(scale_factor_2)
            .shift(LEFT)
        )
        formula = (
            MathTex(r"\frac{\# \sigma}{\# G}", color=YELLOW)
            .next_to(density)
            .scale(scale_factor_2)
        )
        self.play(FadeIn(density, formula))
        self.next_slide()


# Example à refaire
# Mentionner faire plusieurs one-off step


class Example(Slide):
    def construct(self):
        title_example = (
            Title(
                "Examples",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )

        scale_factor = 0.6

        title_1 = (
            Title(
                "A 508 bit-size record on $\mathbb{F}_{p^3}$ using NFS",
                include_underline=True,
                match_underline_width_to_text=True,
            )
            .scale(scale_factor)
            .next_to(title_example, DOWN, aligned_edge=LEFT)
        )
        f_0 = (
            MathTex(r"f_0 = 28x^6 + 16x^5 - 261x^4 - 322x^3 + 79x^2 + 152x + 28")
            .scale(scale_factor)
            .next_to(title_1, DOWN, aligned_edge=LEFT)
        )
        text_1 = (
            Tex("$f_0$ would work as a one-off setup for 4/9 of characteristics.")
            .scale(scale_factor)
            .next_to(f_0, DOWN, aligned_edge=LEFT)
        )
        text_1[0][28:31].set_color(YELLOW)

        title_2 = (
            Title(
                "A 521 bit-size record on $\mathbb{F}_{p^6}$ using TNFS",
                include_underline=True,
                match_underline_width_to_text=True,
            )
            .scale(scale_factor)
            .next_to(text_1, DOWN, aligned_edge=LEFT, buff=0.5)
        )
        h = (
            MathTex(r"h = x^3 - x + 1,")
            .scale(scale_factor)
            .next_to(title_2, DOWN, aligned_edge=LEFT)
        )
        mu = MathTex(r"\mu = x^2-2,").scale(scale_factor).next_to(h, RIGHT)
        f_0_2 = MathTex(r"f_0 = x^4+1").scale(scale_factor).next_to(mu, RIGHT)
        text_2 = (
            Tex("Would work as a one-off setup for 1/12 of characteristics.")
            .scale(scale_factor)
            .next_to(h, DOWN, aligned_edge=LEFT)
        )
        text_2[0][26:30].set_color(YELLOW)

        ant = (
            SVGMobject("ant.svg")
            .scale(0.5)
            .set_fill(WHITE)
            .to_edge(DL)
            .shift(2 * RIGHT)
            .shift(UP)
            .rotate(180, axis=Y_AXIS)
        )

        final_text = (
            Tex(
                "Cover an arbitary large density of primes by doing several one-off steps",
                color=YELLOW,
            )
            .scale(scale_factor)
            .next_to(ant, RIGHT)
        )
        final_text_2 = (
            Tex("Same asymptotic complexity", color=YELLOW)
            .scale(scale_factor)
            .next_to(final_text, DOWN, aligned_edge=LEFT)
        )

        self.add(
            title_example, title_1, f_0, text_1, title_2, h, mu, f_0, f_0_2, text_2
        )
        self.wait()
        self.next_slide()
        self.play(
            FadeIn(ant, final_text, final_text_2),
        )
        self.wait()
        self.next_slide()


class Results(Slide):
    def construct(self):
        title_results = (
            Title(
                "Results: Asymptotic complexities",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        self.add(title_results)

        formula = MathTex(
            r"L_{p^n}\left(\frac{1}{3}, c\right) = e^{ (c+o(1)) \log(p^n)^{1/3} \log\log(p^n)^{2/3} }"
        )
        formula.scale(0.7).to_edge(UP).shift(0.5 * DOWN)
        self.add(formula)
        self.wait()
        self.next_slide()

        table = TexTemplate()
        table.add_to_preamble(r"\usepackage{multirow}")
        table.add_to_preamble(r"\usepackage{makecell}")
        table.add_to_preamble(r"\usepackage{hhline}")
        table.add_to_preamble(r"\renewcommand{\theadfont}{\bfseries}")
        table.add_to_preamble(r"\renewcommand\arraystretch{1.5}")
        table.add_to_preamble(r"\usepackage{xcolor, colortbl}")

        table_code = r"""
        \begin{tabular}{|c||c||c|c|c|c|}
            \hline
            & & & & \multicolumn{2}{c|}{\textbf{Our work} (Factory)} \\
            \thead{Algorithm} & \thead{Range} & \thead{Usual\\approach} & \thead{Multiple\\variant} & \thead{Precomputation\\in each field} & \thead{Computation\\in each field} \\
            \hline
            \hline
            & Prime fields & 1.92 & 1.90 & 2.01~[Barbulescu] & 1.64~[Barbulescu] \\
            \cline{2-6}
            & Large $p$ & 1.92 & 1.90 & \bfseries 2.01 & \bfseries 1.64 \\
            \cline{2-6}
            NFS & $p = L_Q(2/3)$ & \multicolumn{4}{c|}{\bfseries Figure~8 in paper} \\
            \cline{2-6}
            & Medium $p$ & 2.20 & 2.16 & \bfseries 2.45 & \bfseries 1.73 \\
            \arrayrulecolor{yellow}\hline
            TNFS & Medium $p$ & 1.75 & 1.71 & \bfseries 1.94 & \bfseries 1.37 \\
            \arrayrulecolor{yellow}\hline
            SNFS & Large $p$ & 1.53 & - & \bfseries 1.85 & \bfseries 1.39 \\
            \cline{2-6}
            & Medium $p$ & \multicolumn{4}{c|}{\bfseries Table~9 in paper} \\
            \arrayrulecolor{white} \hline
            STNFS & Medium $p$ & \multicolumn{4}{c|}{\bfseries Table~10 in paper} \\
            \hline
        \end{tabular}
        """

        table = Tex(table_code, tex_template=table)
        table.scale(0.6).next_to(formula, DOWN)
        self.play(FadeIn(table))
        self.next_slide()


class Conclusion(Slide):
    def construct(self):
        title_conclusion = (
            Title(
                "Conclusion",
                include_underline=False,
                fill_color=YELLOW,
            )
            .scale(0.7)
            .to_edge(UL)
        )
        self.add(title_conclusion)

        # mini table
        table = TexTemplate()
        table.add_to_preamble(r"\usepackage{multirow}")
        table.add_to_preamble(r"\usepackage{makecell}")
        table.add_to_preamble(r"\usepackage{hhline}")
        table.add_to_preamble(r"\renewcommand{\theadfont}{\bfseries}")
        table.add_to_preamble(r"\renewcommand\arraystretch{1.5}")
        table.add_to_preamble(r"\usepackage{xcolor, colortbl}")
        table_code = r"""
        \begin{tabular}{|c||c||c|c|c|c|}
            \hline
            & & & & \multicolumn{2}{c|}{\textbf{Our work} (Factory)} \\
            \thead{Algorithm} & \thead{Range} & \thead{Usual\\approach} & \thead{Multiple\\variant} & \thead{Precomputation\\in each field} & \thead{Computation\\in each field} \\
            \hline
            \hline
            & Prime fields & 1.92 & 1.90 & 2.01~[Barbulescu] & 1.64~[Barbulescu] \\
            \cline{2-6}
            & Large $p$ & 1.92 & 1.90 & \bfseries 2.01 & \bfseries 1.64 \\
            \cline{2-6}
            NFS & $p = L_Q(2/3)$ & \multicolumn{4}{c|}{\bfseries Figure~8 in paper} \\
            \cline{2-6}
            & Medium $p$ & 2.20 & 2.16 & \bfseries 2.45 & \bfseries 1.73 \\
            \arrayrulecolor{yellow}\hline
            TNFS & Medium $p$ & 1.75 & 1.71 & \bfseries 1.94 & \bfseries 1.37 \\
            \arrayrulecolor{yellow}\hline
            SNFS & Large $p$ & 1.53 & - & \bfseries 1.85 & \bfseries 1.39 \\
            \cline{2-6}
            & Medium $p$ & \multicolumn{4}{c|}{\bfseries Table~9 in paper} \\
            \arrayrulecolor{white} \hline
            STNFS & Medium $p$ & \multicolumn{4}{c|}{\bfseries Table~10 in paper} \\
            \hline
        \end{tabular}
        """
        table = Tex(table_code, tex_template=table)
        table.scale(0.1)

        scale_factor = 0.7

        change_point_of_view = (
            "Target 'almost all' finite fields of a given size and degree."
        )
        better_complexity = (
            "New state-of-the-art complexities given a costly one-time computation."
        )
        practice_estimate = "Estimate: TNFS-Factory becomes profitable when operating on tens of $\mathbb{F}_{p^6}$ of size 1024 bits."
        draw_back = "Drawback: Sub-exponential complexity in memory."
        not_pb = "Not a problem if the finite fields targeted are known in advance."
        blist = (
            BulletedList(
                change_point_of_view,
                better_complexity,
                practice_estimate,
                draw_back,
                not_pb,
                buff=1,
            )
            .scale(scale_factor)
            .next_to(title_conclusion, DOWN, aligned_edge=LEFT)
        )
        blist[0][-14:-10].set_color(GREEN)
        blist[1][0:20].set_color(GREEN)
        blist[1][38:52].set_color(RED)
        blist[2][53:62].set_color(GREEN)
        blist[3][0:10].set_color(RED)
        blist[4][0:12].set_color(GREEN)

        sheeps = VGroup()
        for i in range(5):
            sheeps.add(SVGMobject("sheep.svg").scale(0.2).set_fill(WHITE))
        sheeps.arrange(RIGHT)

        sheeps.next_to(blist[0], RIGHT)

        eprint = (
            Tex("ePrint: eprint.iacr.org/2023/834", color=YELLOW)
            .scale(scale_factor)
            .to_edge(DOWN)
            .shift(UP)
        )

        sheep = (
            SVGMobject("sheep.svg")
            .scale(0.5)
            .set_fill(WHITE)
            .to_edge(DR)
            .shift(1.5 * LEFT)
        )
        Thanks = Tex("Thank you !").scale(0.5).next_to(sheep, UR, buff=-0.1)

        self.add(blist[0], sheeps)
        self.wait()
        self.next_slide()

        table.next_to(blist[1], RIGHT)
        self.play(FadeIn(blist[1], table))
        self.next_slide()

        for i in range(2, len(blist)):
            self.play(FadeIn(blist[i]))
            self.next_slide()
        self.add(eprint)
        self.play(DrawBorderThenFill(sheep), FadeIn(Thanks))
        self.next_slide()
